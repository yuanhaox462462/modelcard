#!/usr/bin/env python3
import io
import pickle
import requests
import tensorflow as tf
import shutil
import fnmatch
import sys, os, urllib3, argparse, pdb
import re
from collections import defaultdict
import pandas as pd

import numpy as np
import pprint

from pathlib import Path
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


from pathlib import Path
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

# Add the location of python-gitlab to the path so we can import it
#repo_top = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
#print('__file__........................................................',__file__)
#print('os.path.realpath(__file__)......................................',os.path.realpath(__file__))
#print('os.path.dirname(os.path.realpath(__file__)).....................',os.path.dirname(os.path.realpath(__file__)))
#print('os.path.join(os.path.dirname(os.path.realpath(__file__)),..)....',os.path.join(os.path.dirname(os.path.realpath(__file__)), '..'))
#print('repo_top........',repo_top)

import gitlab

"""def getArgs():

    parser = argparse.ArgumentParser(description='gitlab project info extraction',formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("token", type=str,default="eKQ1PnYfiXia68M8jf-G", help='token')
    parser.add_argument("project_name", type=str, default="yuanhaox462462/test2",help='username/projectname')
    parser.add_argument("--url", default='https://gitlab.com/', help='https://gitlab.com/')
    parser.add_argument("model", default='model.ckpt', help='The model you would like to downloads and check')

    return parser, parser.parse_args()"""


class GetInfoFromGitlab():
    def __init__(self, url, token, project_name, model):
        # Parse command line arguments
        self.url = url
        self.token = token
        self.model = model
        server = gitlab.Gitlab(self.url, token, api_version=4, ssl_verify=False)
        self.project = server.projects.get(project_name)
        self.user = server.users.list()
        self.contributors = self.project.repository_contributors()
    #self.id = server.id.list()


    def __check_if_file_contains_certain_strings(self, file, check_list):
        """
        This func is to check if the text file contains certain strings in the check list

        :param file: text file
        :param check_list: check list which contains different type of licences
        :return: the licences type or None
        """

        project = self.project
        file1= file
        f = project.files.get(file_path=file1, ref='master')
        keyword=None
        content = f.decode()
        with open("temFile.txt", 'wb') as code:
            code.write(content)
        f1 = open("temFile.txt", 'r')
        lines = f1.readlines()
        for line in lines:
            for keywords in check_list:
                if keywords in line:
                    keyword=keywords
        f1.close()
        return keyword

    #todo 上传100KB以下的文件没有问题，上传184KB大小的文件就开始报500 Internal Server Error

    def __check_if_file_contains_certain_regular_expression(self, file,expression):
        """
        This func is to check if there are certian regular expression in the txt file,
        T和regular expression is the starts of the citation information

        :param file: test file
        :param expression:
        :return:
        """
        project = self.project
        file1= file
        f = project.files.get(file_path=file1, ref='master')
        content = f.decode()
        with open("temFile.txt", 'wb') as code:
            code.write(content)
        f1 = open("temFile.txt", 'r')
        #expression="(@[\s\S]*\}\n\})"
        ret2 = re.findall(expression, f1.read())
        #ret3 = re.search("(?<=@)[\s\S]*}}", str(ret2[0]))
        f1.close()
        if ret2 == []:
            return None
        else:
            return str(ret2[0])
    def data_and_algorithm_info(self):

        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        file = open("MyFile.txt", "a")
        for i in range(len(file1)):
            if file1[i]:
                if "training.py" in file1[i]:
                    TrainingSouceCodeFile = open('training.py')

                    for eachLine in TrainingSouceCodeFile:
                        if ('mode' in eachLine) and ('import' in eachLine):
                            print("The imported model is %s \n"% eachLine)
                            file.write("The imported model is %s\n"% eachLine)
                        if ("csv.reader" in eachLine):
                            UsePythonStandardLibraryToLoadDataset=True
                        elif("loadtxt(" in eachLine ):
                            UseNumpyToLoadDataset = True
                        elif(".read_csv(" in eachLine):
                            UsePandaToLoadDataset=True
                        #todo double check the
                        if (r"http://" in eachLine):
                            (a, afterHTTP) = eachLine.split('http')
                            (beforeCSV, d)=afterHTTP.split('.csv')
                            datasetLink='http'+beforeCSV+'.csv'
                            #file.write(os.path.split(datasetLink))
                            file.write("The datasets is %s can be found by: %s\n" % (os.path.split(datasetLink)[1],datasetLink,))
                            print("The datasets is %s can be found by: %s\n" % (os.path.split(datasetLink)[1],datasetLink,))
                            df = pd.read_csv(datasetLink, sep=';')
                            print(df.info())

                            buffer = io.StringIO()
                            df.info(buf=buffer)
                            s = buffer.getvalue()
                            with open("MyFile.txt", "a",
                                      encoding="utf-8") as f:
                                f.write(s)

                            pd.set_option('display.max_columns', None)
                            # 显示所有行
                            pd.set_option('display.max_rows', None)
                            # 设置value的显示长度为100，默认为50
                            pd.set_option('max_colwidth', 100)
                            file.write("The datasets is %s" % str(df.info()))
                            print(df.describe(include='all'))
                            file.write(str(df.describe(include='all')))
                            DataIsOnline=True

                        #todo：when created locally
                        if('.csv' in eachLine) and (r"http://" not in eachLine):
                            (beforeCSVLocally,z)=eachLine.split('.csv')
                            if(r"'" in eachLine):
                                (x,after)=beforeCSVLocally.split(r"'")

                            else:
                                (x,after)=beforeCSVLocally.split(r'"')
                            datasetLocation = after+'.csv'
                            file.write("The datasets can be found by: %s\n" % datasetLocation)
                            df = pd.read_csv(datasetLocation)
                            file.write("The dataset contains: %s\n" % df.dtypes)
                            DataSavedLocally=True

    def analysis_source_code(self):
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        functionCount=0
        functionNameList=[]
        file = open("MyFile.txt", "a")
        for i in range(len(file1)):
            if file1[i]:
                if "model.py" in file1[i]:
                    modelSouceCodeFile=open('model.py')
                    for eachLine in modelSouceCodeFile:
                        if ('def' in eachLine) and (eachLine.startswith('def')):
                            (beforeDef,afterDef)=eachLine.split('def')

                            (functionName,afterBrace)=afterDef.split('(')
                            functionCount+=1
                            functionNameList.append(functionName)
                    
        file.write("There are %s functions in the model.py: %s\n" % (functionCount,functionNameList))
        print("There are %s functions in the model.py: %s\n" % (functionCount, functionNameList))

        for eachFunction in functionNameList:

            str1='model.'+eachFunction.replace(" ","")+'.__doc__'
            file.write("........................................c")
            file.write(eachFunction.replace(" ",""))
            file.write(eval(str1))

    def nested_dict(self, data, keylist, value):
        nestedDict = data
        for i in keylist[:-1]:
            if i in nestedDict:
                nestedDict = nestedDict[i]
            else:
                nestedDict[i] = {}
                nestedDict = nestedDict[i]

        nestedDict[keylist[-1]] = value

    def extract_info_from_mlflow(self):
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        project = self.project
        params_count = 0
        metric_count = 0
        model_count=0

        dict_all_experiment=dict()
        list_all_experiment_from_params =[]
        list_all_experiment_from_metric=[]
        list_all_experiment_from_model=[]
        for i in range(len(file1)):
            if file1[i]:
                if "params" in file1[i]:
                    dict_temp = {}
                    params_count+=1
                    dict_temp['params_count'] = str(params_count)
                    dict_temp['params_path']=file1[i]
                    input1 = '"' + file1[i] + '"'
                    params_file = file1[i]
                    f = project.files.get(file_path=params_file, ref='master')
                    paramsName = f.file_name
                    dict_temp['params_name'] = paramsName
                    str1=str(f.decode())
                    if str1[0]=='b':
                        dict_temp['params_value'] = str1[1:]
                    else:
                        dict_temp['params_value'] = str1
                    parentPath = str(Path(file1[i]).parent.parent)
                    parentName=parentPath.split('/')[-1]
                    dict_temp['params_parent_file'] = parentName
                    AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                    dict_temp['author'] = AuthorName
                    createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                    dict_temp['createdOn'] = createdOn
                    key=[parentName,'params',paramsName]
                    self.nested_dict(dict_all_experiment, key, dict_temp)
                    list_all_experiment_from_params.append(parentName)
                if "metric" in file1[i]:
                    metric_dict_temp = {}
                    metric_count += 1
                    metric_dict_temp['metric_count'] = str(metric_count)
                    metric_dict_temp['metric_path'] = file1[i]
                    input1 = '"' + file1[i] + '"'
                    params_file = file1[i]
                    f = project.files.get(file_path=params_file, ref='master')
                    metric_name = f.file_name
                    metric_dict_temp['metric_name'] = metric_name
                    str2 = str(f.decode())
                    if str2[0] == 'b':
                        metric_dict_temp['metric_value'] = str2[1:]
                    else:
                        metric_dict_temp['metric_value'] = str2
                    parentPath = str(Path(file1[i]).parent.parent)
                    parentName = parentPath.split('/')[-1]
                    metric_dict_temp['metric_parent_file'] = parentName
                    AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                    metric_dict_temp['author'] = AuthorName
                    createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                    metric_dict_temp['createdOn'] = createdOn
                    key = [parentName, 'metrics', metric_name]
                    self.nested_dict(dict_all_experiment, key, metric_dict_temp)
                    list_all_experiment_from_metric.append(parentName)
                if ".pkl" in file1[i]:
                    model_count+=1
                    modelInfoDict={}
                    model_path=file1[i]
                    f = project.files.get(file_path=model_path, ref='master')
                    file_content = f.decode()
                    with open("pklFile.txt", 'wb') as code:
                        code.write(file_content)
                    f1 = open("pklFile.txt", 'rb')
                    pklFile = pickle.load(f1)
                    model_file = file1[i]
                    f = project.files.get(file_path=model_file, ref='master')
                    model_name = f.file_name
                    parentPath = str(Path(file1[i]).parent.parent.parent)
                    parentName = parentPath.split('/')[-1]
                    input1 = '"' + file1[i] + '"'
                    AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                    modelInfoDict['author'] = AuthorName
                    createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                    modelInfoDict['createdOn'] = createdOn
                    modelInfoDict['parentName']=parentName
                    modelInfoDict['count']=model_count
                    modelInfoDict['name']=model_name
                    modelInfoDict['content']=str(pklFile)
                    key = [parentName, 'model', model_count]
                    self.nested_dict(dict_all_experiment, key, modelInfoDict)
                    list_all_experiment_from_model.append(parentName)

        list_all_experiment=list(set(list_all_experiment_from_params+list_all_experiment_from_params+list_all_experiment_from_model))
        print("There are %d training were tracked"% len(list_all_experiment))
        pprint.pprint(dict_all_experiment)
        with open('MyFile.txt','a') as out:
            out.write("........................................\n")
            out.write("There are %d training were tracked \n"% len(list_all_experiment))
            pprint.pprint(dict_all_experiment, stream=out)

    def read_model(self):

        checkpoint_for_path = self.model
        checkpoint_path = os.path.join(checkpoint_for_path)
        # todo the case if model is not in the root directory and in other type
        print("checkpoint: ", checkpoint_path)
        # Read data from checkpoint file
        reader = tf.train.load_checkpoint(checkpoint_path)
        # reader = pywrap_tensorflow.NewCheckpointReader(checkpfromoint_path)

        var_to_shape_map = reader.get_variable_to_shape_map()
        var_to_dtype_map = reader.get_variable_to_dtype_map()

        var_names = sorted(var_to_shape_map.keys())
        f_model = open("MyFile_model.txt", "w")
        f_model.write("model name: %s\n" % checkpoint_path)
        f_model.write("........................................\n")
        for key in var_names:
            var = reader.get_tensor(key)
            shape = var_to_shape_map[key]
            # dtype = var_to_dtype_map[key]
            # print(key, shape, dtype)
            # print('key:', key, 'shape:', shape, 'mean:', np.mean(var), 'variance:', np.var(var))
            # print('var:', var)
            f_model.write("key: %s, shape: %s, mean: %s, variance: %s\n" % (key, shape, np.mean(var), np.var(var)))
        f_model.close()

    def list_contributors(self):

        contributors = self.contributors
        # contributors_username = contributors[0].get('name')
        # print("contributors : %s" % contributors)
        f = open("MyFile.txt", "w")
        for i in range(len(contributors)):
            contributors_username = contributors[i].get('name')
            contributors_email = contributors[i].get('email')
            contributors_commits = contributors[i].get('commits')
            print("contributors : %s" % contributors_username)
            print("contributors email: %s" % contributors_email)
            print("contributors commits: %s" % contributors_commits)
            f.write("contributors : %s\n" % contributors_username)
            f.write("contributors email: %s\n" % contributors_email)
            f.write("contributors commits: %s\n" % contributors_commits)
            f.write("........................................\n")
        f.close()

    def citation_Info(self):
        f1 = open("MyFile.txt", "a")

        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')

        RM = None
        for i in range(len(file1)):
            if "README" in file1[i]:
                README_file = file1[i]
                # README_list = 'r\'\"\\b@\S+{\\b\"\''
                # README_list = 'r\'@\''
                expression = "(@[\s\S]*\}\n\})"
                RM = self.__check_if_file_contains_certain_regular_expression(README_file, expression)
        if RM:
            f1.write("........................................\n")
            f1.write("Citation: \n %s \n" % RM)
            print("Citation: %s " % RM)
        else:
            f1.write("........................................\n")
            print("Citation: None ")
        f1.close()

    def model_Info(self):
        liscense_exist = False
        model = self.model
        f = open("MyFile.txt", "a")
        # os.system('git ls-tree --name-only HEAD | while read filename; do echo "$(git log -1 --pretty=format:"%ad" -- $filename) $filename" >> File.txt; done')
        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        LIC = None

        for i in range(len(file1)):
            if file1[i]:
                # if "README" in file1[i]:
                if "model" in file1[i]:
                    # todo 大小写
                    if fnmatch.fnmatch(file1[i], '*.ckp'):
                        input1 = '"' + file1[i] + '"'
                        f.write("                model:%s\n" % file1[i])
                        print("modelFile: %s" % file1[i])
                        AuthorName = os.popen('git log -1 --pretty=format:"%%an" -- $"%s"' % input1).readline()
                        Authoremail = os.popen('git log -1 --pretty=format:"%%ae" -- $"%s"' % input1).readline()
                        lastModifed = os.popen('git log -1 --pretty=format:"%%ad" -- $"%s"' % input1).readline()
                        createdOn = os.popen('git log --pretty=format:"%%ad" -- $"%s" | tail -1' % input1).readline()
                        f.write("Author: %s \n" % AuthorName)
                        f.write("Author's email %s \n" % Authoremail)
                        f.write("Create date: %s \n" % createdOn)
                        f.write("Last modifed on: %s \n" % lastModifed)
                        print("Author: %s" % AuthorName)
                        print("Author's email: %s. " % Authoremail)
                        print("Create date: %s, " % createdOn)
                        print("Last modifed on: %s \n" % lastModifed)

                # todo: if there is no license files
                if "LICENSE" in file1[i]:
                    liscense_exist = True
                    f.write("........................................\n")
                    print("There is liscense file in the project")
                    LICENSE_file = file1[i]
                    # todo:liscense type
                    LICENSE_list = ["GPL", "BSD", "MIT", "Mozilla", "Apache", "LGPL", ]
                    LIC = self.__check_if_file_contains_certain_strings(LICENSE_file, LICENSE_list)
        if LIC:
            # f.write("........................................\n")
            f.write("License type: %s, for more information, please check the license file liscense file \n" % LIC)
            print("License type: %s, for more information, please check the license file liscense file" % LIC)
        elif not liscense_exist:
            f.write("........................................\n")
            f.write("There is no license info, please contact developers for the license info.\n ")
            print("There is no license info, please contact developers for the license info ")
        elif liscense_exist and not LIC:
            f.write("For the License info, please check the license file \n")
            print("For the License info, please check the license file")

        f.write("........................................\n")
        f.close()

    def list_projectInfo(self):
        project = self.project
        model = self.model

        f = open("MyFile.txt", "a")

        project_name = project.name
        project_id = project.id
        created_at = project.created_at
        web_url = project.web_url
        readme_url = project.readme_url
        print("project : %s" % project_name)
        print("project id: %s" % project_id)
        print("created at: %s" % created_at)
        print("web_url: %s" % web_url)
        print("readme_url: %s" % readme_url)
        f.write("project : %s\n" % project_name)
        f.write("project id: %s\n" % project_id)
        f.write("created at: %s\n" % created_at)
        f.write("web_url: %s\n" % web_url)
        f.write("readme_url: %s\n" % readme_url)
        f.write("........................................\n")
        f.close()

    def getExperimentsInfo(self):
        response1 = requests.get("http://127.0.0.1:5000/api/2.0/preview/mlflow/experiments/list")
        # todo：set URL， run from git
        print("list all the experimrnts")
        print(response1.content.decode("utf-8"))
        f = open("MyFile.txt", "a")
        f.write("readme_url: %s\n" % response1.content.decode("utf-8"))
        f.close()

    def txt_to_md(self):

        shutil.copyfile("MyFile.txt", "ModelCard.md")


    def run(self):
        self.list_contributors()
        self.list_projectInfo()
        self.model_Info()
        self.citation_Info()
        #self.getExperimentsInfo()

        self.extract_info_from_mlflow()
        #self.analysis_source_code()
        self.data_and_algorithm_info()

        file1 = os.popen('git ls-tree -r --name-only HEAD').read().split('\n')
        for i in range(len(file1)):
            if file1[i]:
                if self.model in file1[i]:
                    self.read_model()

        self.extract_info_from_mlflow()

"""
if __name__ == '__main__':
    token='eKQ1PnYfiXia68M8jf-G'
    project_name="yuanhaox462462/test2"
    url='https://gitlab.com/'
    model='model.ckpt'
    sys.exit(GetInfoFromGitlab(url, token, project_name, model).run())"""