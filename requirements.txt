numpy==1.18.1
python_gitlab==2.2.0
urllib3==1.25.9
tensorflow==2.2.0
requests==2.23.0
gitlab==1.0.2
