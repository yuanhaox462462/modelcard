import tkinter as tk
from tkinter import filedialog
import pandas as pd
import csv
from modelcard.GetInfoFromGitlab import GetInfoFromGitlab
from tkmagicgrid import *
import requests
import tensorflow as tf
import numpy as np
import sys, os, urllib3, argparse, pdb
import re
import io
from PIL import ImageTk, Image
from PIL import Image
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
class gui3:
    def __init__(self, master):

        self.master = master

    def data_citation_infor2_2ndWin(self,window):
        file = open("cit.txt", "a")
        frameCitInfoDisplay = tk.Frame(window, height=30, width=150, bd=2)
        labelCitAuto1Info = tk.LabelFrame(frameCitInfoDisplay, text='Citation Information', labelanchor='n')
        labelCitAuto1Info.pack(side=tk.TOP)

        text = tk.Text(labelCitAuto1Info, height=30, width=150)
        text.place()
        text.pack()

        logPath = "cit.txt"
        log = ""
        with open(logPath, 'r') as f:
            for line in f.readlines():
                log += line
        text.delete(1.0, tk.END)
        text.insert(tk.END, log)
        # root.after(2000, Data)
        frameCitInfoDisplay.grid(row=6, column=0,columnspan=2,padx=5, pady=5, sticky='N' + 'W')


    def data_auto_infor1_2ndWin(self, window):
        frameDataAuto1MetricsInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        labelDataAuto1Info = tk.LabelFrame(frameDataAuto1MetricsInfoDisplay, text='Data Information 3', labelanchor='n')
        labelDataAuto1Info.grid(row=0, column=0,sticky=tk.NSEW)

        gridDataAuto1Use = MagicGrid(labelDataAuto1Info)
        gridDataAuto1Use.grid(row=1, column=0, padx=5, pady=5)
        # gridContributer.pack(side="top", expand=1, fill="both")
        try:
            with io.open("dataInfo1.csv", "r", newline="") as csv_file:
                reader2 = csv.reader(csv_file)
                parsed_rows2 = 1
                for row in reader2:
                    if parsed_rows2 == 1:
                        # Display the first row as a header
                        gridDataAuto1Use.add_header(*row)
                    else:
                        gridDataAuto1Use.add_row(*row)
                    parsed_rows2 += 1
        except:
            pass


        frameDataAuto1MetricsInfoDisplay.grid(row=5, column=0,columnspan=2, padx=5, pady=5,sticky='N' + 'W')

    def data_auto_infor2_2ndWin(self,window):
        file = open("dataInfo2.txt", "a")
        frameDataInfoDisplay = tk.Frame(window, height=30, width=150, bd=2)
        labelDataAuto1Info = tk.LabelFrame(frameDataInfoDisplay, text='Data Information 2', labelanchor='n')
        labelDataAuto1Info.pack(side=tk.TOP)

        text = tk.Text(labelDataAuto1Info, height=30, width=150)
        text.place()
        text.pack()

        logPath = "dataInfo2.txt"
        log = ""
        with open(logPath, 'r') as f:
            for line in f.readlines():
                log += line
        text.delete(1.0, tk.END)
        text.insert(tk.END, log)
        # root.after(2000, Data)
        frameDataInfoDisplay.grid(row=4, column=0,columnspan=2,padx=5, pady=5, sticky='N' + 'W')

    def experiments_infor_2ndWin(self,window):
        file = open("mlflowInfor.txt", "a")
        frameVersioningInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        labelModelInfo = tk.LabelFrame(frameVersioningInfoDisplay, text='Model Information',labelanchor='n')
        labelModelInfo.pack(side=tk.TOP)

        text = tk.Text(labelModelInfo, height=100, width=150)
        text.place()
        text.pack()

        logPath = "mlflowInfor.txt"
        log = ""
        with open(logPath, 'r') as f:
            for line in f.readlines():
                log += line
        text.delete(1.0, tk.END)
        text.insert(tk.END, log)
        # root.after(2000, Data)
        frameVersioningInfoDisplay.grid(row=3, column=0,columnspan=2,padx=5, pady=5, sticky='N' + 'W')

    def metrics_infor_2ndWin(self, window):

        frameMetricsInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        labelMetricsInfo = tk.LabelFrame(frameMetricsInfoDisplay, text='Metrics', labelanchor='n')

        labelMetricsInfo.grid(row=0, column=0, sticky=tk.NSEW)

        gridMetricsUse = MagicGrid(labelMetricsInfo)
        gridMetricsUse.grid(row=1, column=0, padx=5, pady=5)
        # gridContributer.pack(side="top", expand=1, fill="both")
        try:
            with io.open("Metrics_info1.csv", "r", newline="") as csv_file:
                reader2 = csv.reader(csv_file)
                parsed_rows2 = 1
                for row in reader2:
                    if parsed_rows2 == 1:
                        # Display the first row as a header
                        gridMetricsUse.add_header(*row)
                    else:
                        gridMetricsUse.add_row(*row)
                    parsed_rows2 += 1
        except:
            pass

        #labelMetricsInfo.grid(row=1, column=0, padx=5, pady=5,sticky='N'+'W')
        frameMetricsInfoDisplay.grid(row=2, column=1, padx=5, pady=5,sticky=tk.NSEW)

    def data_infor_2ndWin(self, window):
        frameDataInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        labelDataInfo = tk.LabelFrame(frameDataInfoDisplay, text='Data Information 1', labelanchor='n')

        labelDataInfo.grid(row=0, column=0, sticky='N' + 'W')


        gridDataUse = MagicGrid(labelDataInfo)
        gridDataUse.grid(row=1, column=0, padx=5, pady=5)
        # gridContributer.pack(side="top", expand=1, fill="both")
        try:
            with io.open("data_info1.csv", "r", newline="") as csv_file:
                reader2 = csv.reader(csv_file)
                parsed_rows2 = 1
                for row in reader2:
                    if parsed_rows2 == 1:
                        # Display the first row as a header
                        gridDataUse.add_header(*row)
                    else:
                        gridDataUse.add_row(*row)
                    parsed_rows2 += 1
        except:
            pass

        #labelDataInfo.grid(row=1, column=0, padx=5, pady=5,sticky='N'+'W')
        frameDataInfoDisplay.grid(row=2, column=0, padx=5, pady=5,sticky='N'+'W')

    def factor_infor_2ndWin(self, window):
        frameFactorInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        labelFactorInfo = tk.LabelFrame(frameFactorInfoDisplay, text='Factors Information', labelanchor='n')

        labelFactorInfo.grid(row=0, column=0, sticky='N' + 'W')

        gridFactorUse = MagicGrid(labelFactorInfo)
        gridFactorUse.grid(row=1, column=0, padx=5, pady=5)
        # gridContributer.pack(side="top", expand=1, fill="both")
        try:
            with io.open("Factors1.csv", "r", newline="") as csv_file:
                reader2 = csv.reader(csv_file)
                parsed_rows2 = 1
                for row in reader2:
                    if parsed_rows2 == 1:
                        # Display the first row as a header
                        gridFactorUse.add_header(*row)
                    else:
                        gridFactorUse.add_row(*row)
                    parsed_rows2 += 1
        except:
            pass

        #labelFactorInfo.grid(row=0, column=0, padx=5, pady=5,sticky='N'+'W')
        frameFactorInfoDisplay.grid(row=1, column=1, padx=5, pady=5,sticky='N'+'W')





    def intend_use_infor_2ndWin(self, window):
        frameIntendUseInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        labelIntendUseInfo = tk.LabelFrame(frameIntendUseInfoDisplay, text='Intend Use', labelanchor='n')

        labelIntendUseInfo.grid(row=0, column=0, sticky='N' + 'W')

        gridIntendUse = MagicGrid(labelIntendUseInfo)
        gridIntendUse.grid(row=1, column=0, padx=5, pady=5)
        # gridContributer.pack(side="top", expand=1, fill="both")
        try:
            with io.open("Intend_use1.csv", "r", newline="") as csv_file:
                reader2 = csv.reader(csv_file)
                parsed_rows2 = 1
                for row in reader2:
                    if parsed_rows2 == 1:
                        # Display the first row as a header
                        gridIntendUse.add_header(*row)
                    else:
                        gridIntendUse.add_row(*row)
                    parsed_rows2 += 1
        except:
            pass
        frameIntendUseInfoDisplay.grid(row=1, column=0, padx=5, pady=5,sticky='N'+'W')

    def contributor_infor_2ndWin(self, window):
        frameContributorInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        lableContributorInfo = tk.LabelFrame(frameContributorInfoDisplay, text='Contributors Information', labelanchor='n')

        lableContributorInfo.grid(row=0, column=0, sticky='N' + 'W')

        gridContributer = MagicGrid(lableContributorInfo)
        gridContributer.grid(row=1, column=0, padx=7, pady=5)
        # gridContributer.pack(side="top", expand=1, fill="both")
        try:
            with io.open("contributors1.csv", "r", newline="") as csv_file:
                reader2 = csv.reader(csv_file)
                parsed_rows2 = 1
                for row in reader2:
                    if parsed_rows2 == 1:
                        # Display the first row as a header
                        gridContributer.add_header(*row)
                    else:
                        gridContributer.add_row(*row)
                    parsed_rows2 += 1
        except:
            pass

        #lableProjecInfo.grid(row=0, column=0, padx=5, pady=5,sticky='N'+'W')
        frameContributorInfoDisplay.grid(row=0, column=1, padx=5, pady=5,sticky='N'+'W')

    def project_infor_2ndWin(self,window):
        frameProjectInfoDisplay = tk.Frame(window, height=100, width=150, bd=2)
        lableProjecInfo = tk.LabelFrame(frameProjectInfoDisplay, text='Project Information', labelanchor='n')

        lableProjecInfo.grid(row=0, column=0, sticky='N' + 'W')


        grid = MagicGrid(lableProjecInfo)
        grid.grid(row=1, column=0, padx=5, pady=5)
        #grid.pack(side="top", expand=1, fill="both")
        try:
            with io.open("ProjectInfo2.csv", "r", newline="") as csv_file:
                reader = csv.reader(csv_file)
                parsed_rows = 1
                for row in reader:
                    if parsed_rows == 1:
                        # Display the first row as a header
                        grid.add_header(*row)
                    else:
                        grid.add_row(*row)
                    parsed_rows += 1
        except:
            pass

        #lableProjecInfo.grid(row=0, column=0, padx=5, pady=5,sticky='N'+'W')
        frameProjectInfoDisplay.grid(row=0, column=0, padx=5, pady=5,sticky='N'+'W')

    """def user_or_developer(self, master):
        self.frame_1 = tk.Frame(self.master,height=200, width=200,bd=2)
        self.radio_var1 = tk.IntVar()
        fl1=tk.LabelFrame(self.frame_1, text='Are you a developer for this project?',labelanchor='n')
        fl1.pack()
        tk.Radiobutton(fl1, text='Yes', value=1, variable=self.radio_var1,cursor="cross").pack()
        tk.Radiobutton(fl1, text='No', value=2, variable=self.radio_var1,cursor="cross").pack()
        theButton1 = tk.Button(fl1, text="Confirm", command=self.__participant_type1)
        theButton1.pack()
        self.frame_1.pack(padx=10, pady=10,side=tk.LEFT)"""



    def generate_window_2nd(self):
        self.root2 = tk.Toplevel()
        self.root2.title('Model Card')

        #frameWin2 = tk.Frame(self.root2, yscrollcommand=b1.set, height=200, width=200, bd=2)

        self.canvas = tk.Canvas(self.root2)
        scroll_y = tk.Scrollbar(self.root2, orient="vertical", command=self.canvas.yview)

        frame = tk.Frame(self.canvas)


        self.project_infor_2ndWin(frame)

        self.contributor_infor_2ndWin(frame)

        self.intend_use_infor_2ndWin(frame)

        self.factor_infor_2ndWin(frame)

        self.metrics_infor_2ndWin(frame)

        self.data_auto_infor2_2ndWin(frame)

        self.experiments_infor_2ndWin(frame)

        self.data_infor_2ndWin(frame)

        self.data_auto_infor1_2ndWin(frame)
        
        self.data_citation_infor2_2ndWin(frame)

        #self.root2.mainloop()

        self.canvas.create_window(0, 0, anchor='nw', window=frame)
        # make sure everything is displayed before configuring the scrollregion
        self.canvas.update_idletasks()

        self.canvas.configure(scrollregion=self.canvas.bbox('all'),yscrollcommand=scroll_y.set)

        self.canvas.pack(fill='both', expand=True, side='left')
        scroll_y.pack(fill='y', side='right')



        self.root2.mainloop()




        #tk.Button(top, text='出现2级').grid(row=1,column=1,padx=1,pady=1)


    """def In_which_file_you_load_your_datasets_entry_1rtWin(self):
        frame_check_data_load = tk.Frame(self.master, bd=2)
        self.checkDataLoadingEntry = tk.StringVar()
        tk.Button(frame_check_data_load, text='Confirm', command=self.__In_which_file_you_load_your_datasets_entry_callback_1stWin).pack()
        tk.Label(frame_check_data_load, text='Is this the file where you load your data').pack()
        tk.Entry(frame_check_data_load, textvariable=self.checkDataLoadingEntry).pack()

        frame_check_data_load.pack(padx=10, pady=10, side=tk.LEFT)"""

    """def openfn(self):
        filename = filedialog.askopenfilename(title='open')
        print(filename)
        return filename

    def open_img(self,frameFactor):
        x = self.openfn()
        img = Image.open(x)
        img = img.resize((250, 250), Image.ANTIALIAS)
        img = ImageTk.PhotoImage(img)
        panel = tk.Label(frameFactor, image=img)
        panel.image = img
        panel.grid(row=2, column=0, padx=5, pady=5)"""

    def display_mc_button_1stWin(self):
        frame_display = tk.Frame(self.master, bd=2)
        tk.Button(frame_display, text='Generate the model card', command=self.generate_window_2nd).grid(row=3, column=0, padx=5, pady=5, sticky='N')
        frame_display.grid(row=3, column=0, columnspan=2,padx=5, pady=5, sticky='N')




    def intend_use_1stWin(self):
        frame_2 = tk.Frame(self.master, bd=2)
        self.TypedIntendUse = tk.StringVar(value='to predict the wine quality; to test the AutoModelReportTool')
        self.TypedIntendUser = tk.StringVar(value='who would like to try out the AutoModelReportTool')
        self.OutOfScope = tk.StringVar(value='cannot predict the quality by grape types, brand, price')

        fl2 = tk.LabelFrame(frame_2, text='Intended Use',labelanchor='n')
        fl2.pack()

        tk.Label(fl2, text='Primary Intended Uses:').pack()
        tk.Entry(fl2, textvariable=self.TypedIntendUse).pack()
        tk.Label(fl2, text='Domain & Users:').pack()
        tk.Entry(fl2, textvariable=self.TypedIntendUser).pack()
        tk.Label(fl2, text='Out of scope applications:').pack()
        tk.Entry(fl2, textvariable=self.OutOfScope).pack()

        theButton = tk.Button(fl2, text="Save", command=self.__intend_use_1stWin)
        theButton.pack()


        frame_2.grid(row=0, column=0, padx=5, pady=5, sticky='N' + 'W')

    def factor_1stWin(self):
        frameFactor = tk.Frame(self.master, bd=2)
        self.Instrumentation = tk.StringVar(value='All data were captured in a real-world environment with different sensors')
        self.Environment = tk.StringVar()
        self.Groups = tk.StringVar(value='To perform the fairness evaluation, output is median of at least 3 evaluations made by wine experts.')
        self.Attributes = tk.StringVar(value='Model only related to red variants of the Portuguese "Vinho Verde"')
        labelFactors = tk.LabelFrame(frameFactor, text='Factors and Subgroups',labelanchor='n')
        labelFactors.pack()

        tk.Label(labelFactors, text='Instrumentation:').pack()
        tk.Entry(labelFactors, textvariable=self.Instrumentation).pack()
        tk.Label(labelFactors, text='Environment:').pack()
        tk.Entry(labelFactors, textvariable=self.Environment).pack()
        tk.Label(labelFactors, text='Groups').pack()
        tk.Entry(labelFactors, textvariable=self.Groups).pack()
        tk.Label(labelFactors, text='Attributes').pack()
        tk.Entry(labelFactors, textvariable=self.Attributes).pack()

        theButtonFactor = tk.Button(labelFactors, text="Save", command=self.__factor_callback_1stWin)
        theButtonFactor.pack()

        frameFactor.grid(row=0, column=1, padx=5, pady=5, sticky='N' + 'W')

    def metrics_1stWin(self):
        frameMetrics = tk.Frame(self.master, bd=2)
        self.metrics = tk.StringVar(value='R², RMSE, MAE ')


        labelMetrics = tk.LabelFrame(frameMetrics, text='Metrics',labelanchor='n')
        labelMetrics.pack()


        tk.Entry(labelMetrics, textvariable=self.metrics).pack()


        theButton = tk.Button(labelMetrics, text="Save", command=self.__metrics_callback_1stWin)
        theButton.pack()

        frameMetrics.grid(row=1, column=0, padx=5, pady=5, sticky='N' + 'W')

    def data_1stWin(self):
        frameData = tk.Frame(self.master, bd=2)
        self.dataInfo = tk.StringVar(value='Datasets is related to red variants of the Portuguese "Vinho Verde" wine')


        labelData = tk.LabelFrame(frameData, text='Data',labelanchor='n')
        labelData.pack()


        tk.Entry(labelData, textvariable=self.dataInfo).pack()


        theButton = tk.Button(labelData, text="Save", command=self.__data_callback_1stWin)
        theButton.pack()

        frameData.grid(row=1, column=1, padx=5, pady=5, sticky='N' + 'W')

    def access_gitlab_1stWin(self):
        frame_3 = tk.Frame(self.master, bd=2)

        self.myToken = tk.StringVar(value='fSJ7nLyuisYyW2ksdze3')
        self.projectName = tk.StringVar(value='yuanhaox462462/test2')
        self.url1 = tk.StringVar(value='https://gitlab.com/')
        self.model1 = tk.StringVar(value='model.ckpt')
        self.DataLoadingScript = tk.StringVar(value='training.py')
        fl3 = tk.LabelFrame(frame_3, text='Access Info',labelanchor='n')
        fl3.pack()

        tk.Label(fl3, text='myToken:').pack()
        tk.Entry(fl3, textvariable=self.myToken,show='*').pack()
        tk.Label(fl3, text='projectName:').pack()
        tk.Entry(fl3, textvariable=self.projectName).pack()
        tk.Label(fl3, text='url1:').pack()
        tk.Entry(fl3, textvariable=self.url1).pack()
        tk.Label(fl3, text='model:').pack()
        tk.Entry(fl3, textvariable=self.model1).pack()
        tk.Label(fl3, text='DataLoadingScript:').pack()
        tk.Entry(fl3, textvariable=self.DataLoadingScript).pack()

        theButton3 = tk.Button(fl3, text="Confirm", command=self.__access_gitlab_callback_1stWin)
        theButton3.pack()
        frame_3.grid(row=2, column=0, columnspan=2,padx=5, pady=5, sticky='N')

    def __access_gitlab_callback_1stWin(self):
        mt = self.myToken.get()
        pj = self.projectName.get()
        ur = self.url1.get()
        md = self.model1.get()
        dls= str(self.DataLoadingScript.get())

        #print(mt)
        print(pj)
        print(ur)
        print(md)
        print(dls)
        GetInfoFromGitlab(ur, mt, pj, md, dls).run()

        #self.label_text.set(self.TypedIntendUse.get())

    def __intend_use_1stWin(self):
        PrimaryIntendedUses = self.TypedIntendUse.get()
        PrimaryIntendedUsers = self.TypedIntendUser.get()
        OutOfScope = self.OutOfScope.get()
        print(PrimaryIntendedUses)
        print(PrimaryIntendedUsers)
        print(OutOfScope)

        list_intendUse = [PrimaryIntendedUses, PrimaryIntendedUsers, OutOfScope ]
        list_Name = ["Primary Intended Uses", "Primary Intended Users", "Out Of Scope"]
        dataframe = pd.DataFrame({'Item': list_Name, 'Info ': list_intendUse})
        dataframe.to_csv("Intend_use1.csv", index=False, sep=',')
        f = open("MyFile.txt", 'w')
        f.write("Primary Intended Uses: %s\n" % PrimaryIntendedUses)
        f.write("Primary Intended Users: %s\n" % PrimaryIntendedUsers)
        f.write("Out Of Scope: %s\n" % OutOfScope)
        f.write("........................................\n")
        f.close()

    def __metrics_callback_1stWin(self):
        metricsVar = self.metrics.get()
        print(metricsVar)
        list_value = [metricsVar]
        list_Name = ["Metrics Infor"]
        dataframe = pd.DataFrame({'Item': list_Name, 'Info ': list_value})
        dataframe.to_csv("Metrics_info1.csv", index=False, sep=',')
        f = open("MyFile.txt", 'a')
        f.write("Metrics: %s\n" % metricsVar)
        f.write("........................................\n")
        f.close()

    def __data_callback_1stWin(self):
        dataVar = self.dataInfo.get()
        print(dataVar)
        list_value = [dataVar]
        list_Name = ["Data"]
        dataframe = pd.DataFrame({'Item': list_Name, 'Info ': list_value})
        dataframe.to_csv("data_info1.csv", index=False, sep=',')
        f = open("MyFile.txt", 'a')
        f.write("Dinscription of the data: %s\n" % dataVar)
        f.write("........................................\n")
        f.close()

    def __factor_callback_1stWin(self):
        InstrumentationVar=self.Instrumentation.get()
        EnvironmentVar=self.Environment.get()
        GroupsVar=self.Groups.get()
        AttributesVar=self.Attributes.get()
        print(InstrumentationVar)
        print(EnvironmentVar)
        print(GroupsVar)
        print(AttributesVar)

        list_value = [InstrumentationVar, EnvironmentVar, GroupsVar, AttributesVar]
        list_Name = ["Instrumentation", "Environment", "Groups","Attributes"]
        dataframe = pd.DataFrame({'Item': list_Name, 'Info ': list_value})
        dataframe.to_csv("Factors1.csv", index=False, sep=',')
        f = open("MyFile.txt", 'a')
        f.write("Instrumentation: %s\n" % InstrumentationVar)
        f.write("Environment: %s\n" % EnvironmentVar)
        f.write("Groups: %s\n" % GroupsVar)
        f.write("list_value: %s\n" % AttributesVar)
        f.write("........................................\n")
        f.close()

    def __participant_callback_1stWin(self):
        var1=self.radio_var1.get()
        if var1 =='1':
            self.frame_1.destroy()
            with open("MyFile.txt", "r") as f:
                tk.Label(root, text=f.read()).pack()


        print(var1)




    """def display(self):
        self.frameDisplay = tk.Frame(self.master, height=200, width=200, bd=2)


        theButton1 = tk.Button(self.frameDisplay, text="Confirm", command=self.load_project_infor())
        theButton1.pack()
        self.frameDisplay.pack(padx=10, pady=10, side=tk.LEFT)"""


    def run(self):
        self.intend_use_1stWin()
        self.factor_1stWin()
        self.metrics_1stWin()
        self.data_1stWin()
        #self.user_or_developer(self.master)
        self.access_gitlab_1stWin()
        self.display_mc_button_1stWin()
        #self.In_which_file_you_load_your_datasets_entry_1rtWin()
        #self.model_card()
        #self.display()

        #self.do_you_want_to_check_the_modelCard()
        #self.frame_1.destroy()
        #with open("MyFile.txt", "r") as f:
            #tk.Label(root, text=f.read()).pack()

    if __name__ == '__main__':
        root = tk.Tk()

        root.title('Model card')

        APP(root).run()

        root.mainloop()